# Gaia-X DCH Loadbalancer ansible playbook

## Install Ansible
```shell
pip install -r requirements.txt 
```
## Verify you have access to hosts
```shell
ansible all -m ping -i inventory.yml -l loadbalancers -u debian
```

## Apply to hosts
```shell
ansible-playbook haproxy.yml -i inventory.yml -l loadbalancers
```

Note that the OVH DNS credentials should be put in a `credentials.ini` file at the root of the project. For security reasons, it's not included