global
        log /dev/log    local0
        log /dev/log    local1 notice
        chroot /var/lib/haproxy
        stats socket /run/haproxy/admin.sock mode 660 level admin expose-fd listeners
        stats timeout 30s
        user haproxy
        group haproxy
        daemon

        # Default SSL material locations
        ca-base /etc/ssl/certs
        crt-base /etc/ssl/private

        # See: https://ssl-config.mozilla.org/#server=haproxy&server-version=2.0.3&config=intermediate
        ssl-default-bind-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
        ssl-default-bind-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
        ssl-default-bind-options ssl-min-ver TLSv1.2 no-tls-tickets

defaults
        log global
        mode http
        option httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

frontend http-in
    bind *:80
    mode http
    option httplog
	acl http ssl_fc,not
	http-request redirect scheme https if http

frontend https_frontend
    bind *:443 ssl crt-list /etc/haproxy/certs/domains_list.txt
    mode http
    option httpclose

    acl host_registry hdr_end(host) -i registry.gaia-x.eu
    acl host_compliance hdr_end(host) -i compliance.gaia-x.eu
    acl host_notary hdr_end(host) -i registrationnumber.notary.gaia-x.eu
    acl host_notary hdr_end(host) -i notary.gaia-x.eu

    use_backend back-registry-deltadao if host_registry { rand(15),mod(10) -m int 0 } { nbsrv(back-registry-deltadao) gt 0 }
    use_backend back-registry-neusta-aerospace if host_registry { rand(15),mod(10) -m int 0 } { nbsrv(back-registry-neusta-aerospace) gt 0 }
    use_backend back-registry if host_registry
    use_backend back-compliance-deltadao if host_compliance { rand(15),mod(10) -m int 0 } { nbsrv(back-compliance-deltadao) gt 0 }
    use_backend back-compliance-neusta-aerospace if host_compliance { rand(15),mod(10) -m int 0 } { nbsrv(back-compliance-neusta-aerospace) gt 0 }
    use_backend back-compliance if host_compliance
    use_backend back-notary-deltadao if host_notary { rand(15),mod(10) -m int 0 } { nbsrv(back-notary-deltadao) gt 0 }
    use_backend back-notary-neusta-aerospace if host_notary { rand(15),mod(10) -m int 0 } { nbsrv(back-notary-neusta-aerospace) gt 0 }
    use_backend back-notary if host_notary

    default_backend back-registry

backend back-registry
    option httpclose
    option httpchk
    http-send-name-header Host
	redirect scheme https if !{ ssl_fc }
	server gx-registry.airenetworks.es gx-registry.airenetworks.es:443 ssl verify none
	server gx-registry.arsys.es gx-registry.arsys.es:443 ssl verify none
	server gx-registry.aruba.it gx-registry.aruba.it:443 ssl verify none
	server gx-registry.gxdch.dih.telekom.com gx-registry.gxdch.dih.telekom.com:443 ssl verify none
	server registry.gxdch.gaiax.ovh registry.gxdch.gaiax.ovh ssl verify none
	server gx-registry.gxdch.proximus.eu gx-registry.gxdch.proximus.eu ssl verify none
	server portal.pfalzkom-gxdch.de portal.pfalzkom-gxdch.de ssl verify none
	server registry.cispe.gxdch.clouddataengine.io registry.cispe.gxdch.clouddataengine.io ssl verify none
	## LAB
	server registry.lab.gaia-x.eu registry.lab.gaia-x.eu:443 ssl verify none

backend back-registry-deltadao
    option httpclose
    option httpchk
    http-send-name-header Host
    http-request replace-path /(.*) /registry/\1
	redirect scheme https if !{ ssl_fc }
	server www.delta-dao.com www.delta-dao.com:443 ssl verify none

backend back-registry-neusta-aerospace
    option httpclose
    option httpchk
    http-send-name-header Host
    http-request replace-path /(.*) /registry/\1
	redirect scheme https if !{ ssl_fc }
	server aerospace-digital-exchange.eu aerospace-digital-exchange.eu:443 ssl verify none

backend back-compliance
    option httpclose
    option httpchk
    http-send-name-header Host
	redirect scheme https if !{ ssl_fc }
	server gx-compliance.airenetworks.es gx-compliance.airenetworks.es:443 ssl verify none
	server gx-compliance.arsys.es gx-compliance.arsys.es:443 ssl verify none
	server gx-compliance.aruba.it gx-compliance.aruba.it:443 ssl verify none
	server gx-compliance.gxdch.dih.telekom.com gx-compliance.gxdch.dih.telekom.com:443 ssl verify none
	server compliance.gxdch.gaiax.ovh compliance.gxdch.gaiax.ovh ssl verify none
	server gx-compliance.gxdch.proximus.eu gx-compliance.gxdch.proximus.eu ssl verify none
	server compliance.pfalzkom-gxdch.de compliance.pfalzkom-gxdch.de ssl verify none
	server compliance.cispe.gxdch.clouddataengine.io compliance.cispe.gxdch.clouddataengine.io ssl verify none
	## LAB
	server compliance.lab.gaia-x.eu compliance.lab.gaia-x.eu:443 ssl verify none

backend back-compliance-deltadao
    option httpclose
    option httpchk
    http-send-name-header Host
    http-request replace-path /(.*) /compliance/\1
	redirect scheme https if !{ ssl_fc }
	server www.delta-dao.com www.delta-dao.com:443 ssl verify none

backend back-compliance-neusta-aerospace
    option httpclose
    option httpchk
    http-send-name-header Host
    http-request replace-path /(.*) /compliance/\1
	redirect scheme https if !{ ssl_fc }
	server aerospace-digital-exchange.eu aerospace-digital-exchange.eu:443 ssl verify none


backend back-notary
    option httpclose
    option httpchk
    http-send-name-header Host
	redirect scheme https if !{ ssl_fc }
	server gx-notary.airenetworks.es gx-notary.airenetworks.es:443 ssl verify none
	server gx-notary.arsys.es gx-notary.arsys.es:443 ssl verify none
    server gx-notary.aruba.it gx-notary.aruba.it:443 ssl verify none
	server gx-notary.gxdch.dih.telekom.com gx-notary.gxdch.dih.telekom.com:443 ssl verify none
	server notary.gxdch.gaiax.ovh notary.gxdch.gaiax.ovh ssl verify none
	server gx-notary.gxdch.proximus.eu gx-notary.gxdch.proximus.eu ssl verify none
	server trust-anker.pfalzkom-gxdch.de trust-anker.pfalzkom-gxdch.de ssl verify none
	server notary.cispe.gxdch.clouddataengine.io notary.cispe.gxdch.clouddataengine.io ssl verify none
	## LAB
	server registrationnumber.notary.lab.gaia-x.eu registrationnumber.notary.lab.gaia-x.eu:443 ssl verify none

backend back-notary-deltadao
    option httpclose
    option httpchk
    http-send-name-header Host
    http-request replace-path /(.*) /notary/\1
	redirect scheme https if !{ ssl_fc }
	server www.delta-dao.com www.delta-dao.com:443 ssl verify none

backend back-notary-neusta-aerospace
    option httpclose
    option httpchk
    http-send-name-header Host
    http-request replace-path /(.*) /notary/\1
	redirect scheme https if !{ ssl_fc }
	server aerospace-digital-exchange.eu aerospace-digital-exchange.eu:443 ssl verify none
