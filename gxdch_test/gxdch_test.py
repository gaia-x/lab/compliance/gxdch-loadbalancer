import json
import sys
from urllib.parse import urlparse
from posixpath import join as urljoin
import requests
from cryptography import x509

components_url = {'compliance': '', 'registry': '', 'notary': ''}
componentCert = {'compliance': '', 'notary': ''}


def syntax():
    raise SyntaxError(
        "Syntax: python3 gxdch.py complianceUrl=https://... registryUrl=https://... notaryUrl=https://...")


def submit_post_req_json(url, json):
    try:
        return requests.post(url, json=json, timeout=10)
    except requests.exceptions.Timeout:
        print("Timed out while calling " + url)
        raise ValueError("Timed out while calling " + url)


def submit_post_req(url, data):
    try:
        return requests.post(url, data, timeout=10)
    except requests.exceptions.Timeout:
        print("Timed out while calling " + url)
        raise ValueError("Timed out while calling " + url)


def submit_get_req(url):
    try:
        return requests.get(url, timeout=0.8)
    except requests.exceptions.Timeout:
        print("Timed out while calling " + url)
        raise ValueError("Timed out while calling " + url)


def get_did_uri(component_url):
    if urlparse(component_url).path in ('', '/'):
        return urljoin(component_url, ".well-known/did.json")
    else:
        return urljoin(component_url, "did.json")


def get_cert_common_names(cert):
    try:
        names = cert.subject.get_attributes_for_oid(x509.NameOID.COMMON_NAME)
        return names[0].value if names else ""
    except x509.ExtensionNotFound:
        return ""


def get_alt_names(cert):
    try:
        ext = cert.extensions.get_extension_for_class(x509.SubjectAlternativeName)
        return ext.value.get_values_for_type(x509.DNSName)
    except x509.ExtensionNotFound:
        return []


def is_domain_included_in_certificate_names(domain, certificates_names):
    for cert_name in certificates_names:
        if cert_name == domain or wildcard_matches_domain(domain, cert_name):
            return True
    return False


def wildcard_matches_domain(domain, cert_name):
    try:
        if cert_name.index("*.") > -1:
            return cert_name[cert_name.index("."):] == domain[domain.index("."):]
    except ValueError:
        pass
    return False


def submit_lrn_request(lrn_request):
    r = submit_post_req_json(urljoin(components_url['notary'], "registrationNumberVC"), json.loads(lrn_request))
    assert r.status_code == 200
    response = r.json()
    assert response['id'] is not None, "LRN VC ID is missing from response"
    assert response['issuer'] is not None, "LRN VC issuer is missing from response"
    assert response['evidence'] is not None, "LRN VC evidence is missing from response"
    assert response['proof'] is not None and response['proof'][
        'verificationMethod'] is not None, "LRN VC proof is missing from response"
    assert response['issuer'] in response['proof']['verificationMethod'], "LRN VC proof and issuer mismatch"


def it_should_be_deployed(component_url):
    r = submit_get_req(component_url)
    assert r.status_code == 200, "Component " + component_url + ' answered != 200 on HTTP request'
    print("PASS: " + component_url + " seems deployed")


def it_should_display_version_on_homepage(component_url):
    r = submit_get_req(component_url)
    component_version = r.json()["version"]
    print("PASS: " + component_url + " is in version # " + component_version)


def it_should_expose_the_documentation_on_slash_docs(component_url):
    print(urljoin(component_url, "docs"))
    r = submit_get_req(urljoin(component_url, "docs"))
    assert r.status_code == 200 and "swagger" in r.text, "Swagger on " + component_url + " seems to not be accessible"
    print("PASS: " + urljoin(component_url, "docs") + " seems to be a swagger")


def it_should_expose_did(component_url):
    did_url = get_did_uri(component_url)
    r = submit_get_req(did_url)
    assert r.status_code == 200, "Component's " + component_url + " DID is not accessible"
    print("PASS: " + component_url + " exposes a DID at " + did_url)


def it_should_have_a_valid_did(component_url):
    did_url = get_did_uri(component_url)
    r = submit_get_req(did_url)
    did_content = r.json()
    assert did_content["verificationMethod"] is not None, "DID " + did_url + " is invalid, no verificationMethod"
    for verificationMethod in did_content["verificationMethod"]:
        if verificationMethod["publicKeyJwk"] is not None and verificationMethod["publicKeyJwk"]["x5u"] is not None:
            print("PASS: " + component_url + " DID seems to contain minimal did information")
            return verificationMethod["publicKeyJwk"]["x5u"]
    raise ValueError("No verification method with an x5u found in the did " + did_url)


def it_should_have_a_leaf_cert_that_matches_deployment_host(component_url, cert):
    common_names = get_cert_common_names(cert)
    alternate_names = get_alt_names(cert)
    print(component_url + " cert CN: = " + common_names + " Alt names:" + ",".join(alternate_names))
    domain = urlparse(component_url).hostname
    alternate_names.append(common_names)
    if is_domain_included_in_certificate_names(domain, alternate_names):
        print("PASS: " + component_url + " cert leaf matches ")
    else:
        print("WARN: " + component_url + " cert leaf does not match deployed url ")


def it_should_have_a_valid_certificate_in_did_x5u_field(component_url, certificate_uri):
    r = requests.get(certificate_uri)
    certs = x509.load_pem_x509_certificates(str.encode(r.text))
    it_should_have_a_leaf_cert_that_matches_deployment_host(component_url, certs[0])


def it_should_validate_compliance_and_notary_certificate_validity_in_registry():
    print(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'))
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": componentCert['compliance']})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "provider compliance cert in provider's registry invalid " + r.text
    print("PASS: Provider compliance certificate is validated against registry")
    r = submit_post_req(components_url['registry'] + '/api/trustAnchor/chain/file', {"uri": componentCert['notary']})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "provider's notary cert in provider's registry invalid " + r.text
    print("PASS: Provider notary certificate is validated against registry")


def it_should_validate_aisbl_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://registration.lab.gaia-x.eu/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks AISBL cert as invalid " + r.text
    print("PASS: AISBL notary certificate is validated against registry")


def it_should_validate_tsys_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://gx-notary.gxdch.dih.telekom.com/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks TSYS cert as invalid " + r.text
    print("PASS: TSYS notary certificate is validated against registry")


def it_should_validate_aruba_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://gx-notary.aruba.it/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks Aruba cert as invalid " + r.text
    print("PASS: Aruba notary certificate is validated against registry")


def it_should_validate_aires_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://gx-notary.airenetworks.es/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks Aires cert as invalid " + r.text
    print("PASS: Aires notary certificate is validated against registry")


def it_should_validate_arsys_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://gx-notary.arsys.es/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks Arsys cert as invalid " + r.text
    print("PASS: Arsys notary certificate is validated against registry")


def it_should_validate_deltadao_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://www.delta-dao.com/notary/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks DeltaDAO cert as invalid " + r.text
    print("PASS: DeltaDAO notary certificate is validated against registry")


def it_should_validate_neusta_aerospace_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://aerospace-digital-exchange.eu/notary/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks Neusta Aerospace cert as invalid " + r.text
    print("PASS: DeltaDAO notary certificate is validated against registry")


def it_should_validate_ovh_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://notary.gxdch.gaiax.ovh/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks OVH cert as invalid " + r.text
    print("PASS: OVH notary certificate is validated against registry")


def it_should_validate_proximus_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://gx-notary.gxdch.proximus.eu/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks Proximus cert as invalid " + r.text
    print("PASS: Proximus notary certificate is validated against registry")

def it_should_validate_pfalzkom_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://trust-anker.pfalzkom-gxdch.de/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks Pfalzkom cert as invalid " + r.text
    print("PASS: Pfalzkom notary certificate is validated against registry")

def it_should_validate_cispe_notary_certificate_validity_in_registry():
    r = submit_post_req(urljoin(components_url['registry'], 'api/trustAnchor/chain/file'),
                        {"uri": "https://notary.cispe.gxdch.clouddataengine.io/v1/.well-known/x509CertificateChain.pem"})
    assert r.status_code == 200 and r.json()[
        'result'] is True, "Provider's registry marks Cispe cert as invalid " + r.text
    print("PASS: Cispe notary certificate is validated against registry")


def it_should_not_validate_vp_from_development():
    with open("./payloads/development_vp.json", "r") as f:
        verifiable_presentation = f.read()
    r = submit_post_req(urljoin(components_url['compliance'], "api/credential-offers"), verifiable_presentation)
    assert (r.status_code == 409 and "X509 certificate chain could not be resolved against registry trust anchors "
            in r.text), "The compliance engine allowed an invalid VP " + r.text
    print("PASS: Compliance properly refused a development VP")


def it_should_validate_vp_properly_signed():
    with open("./payloads/prod_vp.json", "r") as f:
        verifiable_presentation = f.read()
    r = submit_post_req(urljoin(components_url['compliance'], "api/credential-offers"), verifiable_presentation)
    assert r.status_code == 201, ("The compliance engine did not allow a VP with production credentials, something's "
                                  "wrong with the engine") + r.text
    print("PASS: Compliance properly issued a complianceVC from a prod VP")


def it_should_issue_proper_legal_registration_number_from_vat_number():
    with open("./payloads/lrn.json", "r") as f:
        lrn_request = f.read()
    try:
        submit_lrn_request(lrn_request)
        print("PASS notary issues what seems to be a valid LRN from VAT")
    except AssertionError:
        print("FAIL notary issues an invalid LRN from VAT")
        raise AssertionError


def it_should_issue_proper_legal_registration_number_from_vat_number2():
    with open("./payloads/lrn2.json", "r") as f:
        lrn_request = f.read()
    try:
        submit_lrn_request(lrn_request)
        print("PASS notary issues what seems to be a valid LRN from VAT")
    except AssertionError:
        print("FAIL notary issues an invalid LRN from VAT")
        raise AssertionError


def it_should_issue_proper_legal_registration_number_from_eori():
    with open("./payloads/lrn_eori.json", "r") as f:
        lrn_request = f.read()
    try:
        submit_lrn_request(lrn_request)
        print("PASS notary issues what seems to be a valid LRN from EORI")
    except AssertionError:
        print("FAIL notary issues an invalid LRN from EORI")
        raise AssertionError


def it_should_issue_proper_legal_registration_number_from_lei_code():
    with open("./payloads/lrn_lei.json", "r") as f:
        lrn_request = f.read()
    try:
        submit_lrn_request(lrn_request)
        print("PASS notary issues what seems to be a valid LRN from LeiCode")
    except AssertionError:
        print("FAIL notary issues an invalid LRN from LeiCode")
        raise AssertionError


def main(**kwargs):
    if kwargs.get("complianceUrl") is None or kwargs.get("registryUrl") is None or kwargs.get("notaryUrl") is None:
        syntax()
        exit()
    components_url['compliance'] = kwargs.get("complianceUrl")
    components_url['registry'] = kwargs.get("registryUrl")
    components_url['notary'] = kwargs.get("notaryUrl")

    for component_name, url in components_url.items():
        print("===== EVALUATING " + component_name + " =====")
        it_should_be_deployed(url)
        it_should_display_version_on_homepage(url)
        it_should_expose_the_documentation_on_slash_docs(url)
        if component_name in ["compliance", "notary"]:
            it_should_expose_did(url)
            x5u_value = it_should_have_a_valid_did(url)
            componentCert[component_name] = x5u_value
            it_should_have_a_valid_certificate_in_did_x5u_field(url, x5u_value)
        print("===== EVALUATION " + component_name + " DONE =====")
    it_should_validate_compliance_and_notary_certificate_validity_in_registry()
    it_should_validate_aisbl_notary_certificate_validity_in_registry()
    it_should_validate_tsys_notary_certificate_validity_in_registry()
    it_should_validate_aires_notary_certificate_validity_in_registry()
    it_should_validate_aruba_notary_certificate_validity_in_registry()
    it_should_validate_arsys_notary_certificate_validity_in_registry()
    it_should_validate_deltadao_notary_certificate_validity_in_registry()
    it_should_validate_ovh_notary_certificate_validity_in_registry()
    it_should_validate_neusta_aerospace_notary_certificate_validity_in_registry()
    it_should_validate_proximus_notary_certificate_validity_in_registry()
    it_should_validate_pfalzkom_notary_certificate_validity_in_registry()
    it_should_validate_cispe_notary_certificate_validity_in_registry()
    try:
        it_should_issue_proper_legal_registration_number_from_vat_number()
    except AssertionError:
        it_should_issue_proper_legal_registration_number_from_vat_number2()
    it_should_issue_proper_legal_registration_number_from_eori()
    it_should_issue_proper_legal_registration_number_from_lei_code()
    it_should_not_validate_vp_from_development()
    it_should_validate_vp_properly_signed()


if __name__ == "__main__":
    if len(sys.argv) < 4:
        syntax()
    # noinspection PyTypeChecker
    main(**dict(arg.split('=') for arg in sys.argv[1:]))
