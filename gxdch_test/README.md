# GXDCH tests

This project allows to execute tests on a GXDCH deployed and accessible over the internet

## Requirements 

This project runs using Python3. Needed dependencies can be installed using pip

```bash
pip install -r requirements.txt
```

## Usage
```shell
python3 gxdch_test.py complianceUrl=https://... registryUrl=https://... notaryUrl=https://...
```


Airesnetwork:
```shell
python3 gxdch_test.py complianceUrl=https://gx-compliance.airenetworks.es/v1 registryUrl=https://gx-registry.airenetworks.es/v1 notaryUrl=https://gx-notary.airenetworks.es/v1
```

Arsys:
```shell
python3 gxdch_test.py complianceUrl=https://gx-compliance.arsys.es/v1 registryUrl=https://gx-registry.arsys.es/v1 notaryUrl=https://gx-notary.arsys.es/v1
```

Aruba:
```shell
python3 gxdch_test.py complianceUrl=https://gx-compliance.aruba.it/v1 registryUrl=https://gx-registry.aruba.it/v1 notaryUrl=https://gx-notary.aruba.it/v1
```

DeltaDAO:
```shell
python3 gxdch_test.py complianceUrl=https://www.delta-dao.com/compliance/v1 registryUrl=https://www.delta-dao.com/registry/v1 notaryUrl=https://www.delta-dao.com/notary/v1
```

Neusta Aerospace:
```shell
python3 gxdch_test.py complianceUrl=https://aerospace-digital-exchange.eu/compliance/v1 registryUrl=https://aerospace-digital-exchange.eu/registry/v1 notaryUrl=https://aerospace-digital-exchange.eu/notary/v1
```

OVHCloud:
```shell
python3 gxdch_test.py complianceUrl=https://compliance.gxdch.gaiax.ovh/v1 registryUrl=https://registry.gxdch.gaiax.ovh/v1 notaryUrl=https://notary.gxdch.gaiax.ovh/v1
```

T-Systems:
```shell
python3 gxdch_test.py complianceUrl=https://gx-compliance.gxdch.dih.telekom.com/v1 registryUrl=https://gx-registry.gxdch.dih.telekom.com/v1 notaryUrl=https://gx-notary.gxdch.dih.telekom.com/v1
```
