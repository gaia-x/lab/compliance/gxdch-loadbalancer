from unittest import TestCase

import gxdch_test


class TestDomainInCertificatesName(TestCase):
    def test_it_should_return_true_when_domain_in_certificates_names(self):
        self.assertTrue(
            gxdch_test.is_domain_included_in_certificate_names("domain.com", ["domain.com", "sub.domain.com"]))

    def test_it_should_return_true_when_subdomain_in_certificates_names(self):
        self.assertTrue(
            gxdch_test.is_domain_included_in_certificate_names("sub.domain.com", ["domain.com", "sub.domain.com"]))

    def test_it_should_return_true_when_subdomain_in_certificates_names_as_wildcard_subdomain(self):
        self.assertTrue(
            gxdch_test.is_domain_included_in_certificate_names("sub.domain.com", ["domain.com", "*.domain.com"]))

    def test_is_domain_included_in_certificate_names_with_level3_subdomain_and_wildcard_level2(self):
        self.assertFalse(
            gxdch_test.is_domain_included_in_certificate_names("sub.sub2.domain.com", ["domain.com", "*.domain.com"]))

    def test_it_should_return_true_when_subsubdomain_in_certificates_names_as_wildcard_subsubdomain(self):
        self.assertTrue(
            gxdch_test.is_domain_included_in_certificate_names("sub.sub2.domain.com",
                                                               ["domain.com", "*.sub2.domain.com"]))
