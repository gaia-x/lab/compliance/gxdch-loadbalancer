# Software System

[[_TOC_]]

## Tagus release
### Context
```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/tagus/softwareSystem.puml
@enduml
```

### Container
```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/tagus/container.puml
@enduml
```
### Components
#### Notary
```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/tagus/component_notary.puml
@enduml
```
#### Registry
```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/tagus/component_registry.puml
@enduml
```
#### Compliance
```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/tagus/component_compliance.puml
@enduml
```

## Loire release
### Context
```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/loire/softwareSystem.puml
@enduml
```

### Container

```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/loire/container.puml
@enduml
```
### Components
#### Notary

```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/loire/component_notary.puml
@enduml
```
#### Registry
```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/loire/component_registry_ipfs.puml
@enduml
```

```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/loire/component_registry_ipfs_pinning.puml
@enduml
```
#### Compliance
```plantuml
@startuml
!include https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/architecture/loire/component_compliance.puml
@enduml
```
