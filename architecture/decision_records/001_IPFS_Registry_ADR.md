# ADR 001: IPFS pinning & Registry

[[_TOC_]]

## Context
For the Loire release, the LAB aims to use IPFS as a distributed backend storage for the Gaia-X ontology (including JSON-LD schemas & SHACL shapes)

The goal of this ADR is to specify:
- What is the software lifecycle of the IPFS Pinning Service
- What is the software deployment architecture of the IPFS Pinning Service
- What is the process to provide to production instances of the registry the updated version of the ontology
- What kind of DNS records are going to be used/created to answer the previous questions
- Provide explanations on the technical choices made by the team

## Software lifecycle of the IPFS Pinning Service & deployment

Proposals:
### The IPFS pinning service follows no specific lifecycle. The development branch is the main one used, and the pushes from the SvcCh WG repo triggers its deployment, whether it's a development ontology or a new release
This means that at any time, only one instance is deployed, and pins artifacts for all versions of the ontology
### The IPFS pinning service follows the same lifecycle as others components (compliance, registry) with semantic versioning and conventional commits
This means that at any time, n instances are deployed. Development being the latest and most updated, main being the latest stable version, and v1/v2/vn the latest stable releases

Decision: The [IPFS Pinning Service](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning) will follow the same lifecycle as others components with semantic versioning and conventional commits

## Managing registry needs to have production & non-production versions of the ontology

Proposals:
### The IPFS pinning service pins at all time all the ontologies versions
Each Pinning Service pins its root static folder, and under it all versions of the ontology existing
- /static
  - /development
    - /shapes
    - /schemas
  - /2210
    - /shapes
    - /schemas
  - ...
### Each instance of the IPFS Pinning Service pins a single version of the ontology
Each Pinning Service pins a designated folder for a version of the ontology
- /static
    - /2210
        - /shapes
        - /schemas

Either way, the registry service has to integrate a mechanism to exclude ontologies version he can't manage

Decision: The [IPFS Pinning Service](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning) pins at all time all the ontologies versions


## DNS records used by the IPFS Pinning Service
The goal of having understandable DNS records is to allow third party implementations

Proposals:
### The pinning service pins on domains containing the ontology version
The pinning service will ping the 2404 ontology under 2404.gxdch.eu, and so on
This proposal is not compatible with each pinning service containing all ontologies versions. 
And it's not compatible with having more than 1 instance running per version of the ontology 

### The pinning service pins based on its namespace
If following the "classic" Gaia-X deployment, it means that:
- development pinning service will pin on development.gxdch.eu
- v2 pinning service will ping on v2.development.gxdch.eu

Decision: The [IPFS Pinning Service](https://gitlab.com/gaia-x/lab/compliance/gx-ipfs-pinning) pins based on its namespace

## Workflow proposal for an IPFS Pinning service running on all environments (dev/main/v2) and pinning all versions of the ontology
```plantuml
@startuml
'https://plantuml.com/sequence-diagram
Actor "Service Characteristics WG" as SvcChWG
Database "Service Characteristics WG Repository" as SvcChWGRepository
Database "IPFS Pinning Repository" as IPFSPinningRepository
Entity "IPFS Pinning instance" as IPFSPinningInstance
Database "OctoDNS Repository" as OctoDNSRepository
Database "DNS" as DNS
Entity "Registry instance" as RegistryInstance
Database "IPFS"
Actor "End user" as EndUser

group 1 pinning service = all ontologies
    group development
    SvcChWG -> SvcChWGRepository : pushes changes on the ontology
    SvcChWGRepository -> IPFSPinningRepository : Gitlab CI: builds artifacts and \npushes to in /static/development
    IPFSPinningRepository -> IPFSPinningInstance : Gitlab CI: deploys the new pinning \ninstance on development namespace
    IPFSPinningInstance -> IPFSPinningInstance: Computes CIDs
    IPFSPinningInstance -> OctoDNSRepository: Commits a change in DNS \nfor development.gxdch.eu on
    OctoDNSRepository -> DNS: pushes changes on nameservers
    RegistryInstance -> DNS: Reads DNS records based on \nDOMAIN configuration ex: development.gxdch.eu
    RegistryInstance -> IPFS: Gets artifacts from the network
    RegistryInstance -> RegistryInstance: Excludes some artifacts from being queried
    EndUser -> RegistryInstance: Gets artifacts for a document version or "development/latest"\n/api/shapes/2210 or /api/shapes
    end
    group tag/release
    SvcChWG -> SvcChWGRepository : tags changes on the ontology
    SvcChWGRepository -> IPFSPinningRepository : Gitlab CI: builds artifacts and \npushes to in /static/{tag}
    IPFSPinningRepository -> IPFSPinningInstance : Gitlab CI: deploys the new pinning \ninstance on development namespace
    IPFSPinningInstance -> IPFSPinningInstance: Computes CIDs
    IPFSPinningInstance -> OctoDNSRepository: Commits a change in DNS \nfor development.gxdch.eu on
    OctoDNSRepository -> DNS: pushes changes on nameservers
    RegistryInstance -> DNS: Reads DNS records based on \nDOMAIN configuration eg: development.gxdch.eu
    RegistryInstance -> IPFS: Gets artifacts from the network
    RegistryInstance -> RegistryInstance: Excludes some artifacts from being queried
    EndUser -> RegistryInstance: Gets artifacts for a document version or "development/latest"\n/api/shapes/2404 or /api/shapes
    end
end
note over RegistryInstance
    The registry holds the version number
     of ontologies he's able to serve
     It contains a parameter allowing to define the subdomain he will
     query to get his artifacts. Allows to decouple from env & from
     the IPFS Pinning Service version/env
end note
@enduml
```

## Diagram proposal for an IPFS Pinning service running on one environment and pinning all versions of the ontology

![Diagram](./adr_001_diagram.png)