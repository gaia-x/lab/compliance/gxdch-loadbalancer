# ADR 002: Notary Registration Number Endpoint

> Tuesday 28th of May 2024

[[_TOC_]]

## Context

During the new Service Characteristics Working Group ontology integration into the Gaia-X Notary, we noticed that a few
adjustments were needed to make the endpoint fully compatible with the new registration number classes.

The current endpoint uses a single input object that contains all the registration number values which is sent through
a _POST_ request to the Notary. It then outputs a single verifiable credential containing all the verified registration
number values.

The registration number endpoint is now a year old, and we have some experience with it so it's a good time to implement
our desired improvements. Furthermore, our goal is the support OpenID Connect for Verifiable Credentials (OIDC4VC) which
must be kept in mind during the following changes.

## Options

### Lightly Adjust the Current Endpoint

| 👍 Pros                                           | 👎 Cons                                                                              |
|---------------------------------------------------|--------------------------------------------------------------------------------------|
| Less changes                                      | Single input/output for multiple registration numbers                                |
| Easier for end-users                              | New registration number = New verifiable credential with a new signature             |
| Single verifiable credential output (ease of use) | Not compliant with the new ontology (single registration number vs. one per type)    |
|                                                   | Notary must go through each registration number type even those who aren't requested |

### Multiple Credential Subjects in Output

| 👍 Pros                                             | 👎 Cons                                                                              |
|-----------------------------------------------------|--------------------------------------------------------------------------------------|
| Single verifiable credential                        | Single output for multiple registration numbers                                      |
| Possibility to have multiple input query parameters | Not compatible with most OIDC4VC wallets                                             |
| Compliant with the new ontology                     | New registration number = New verifiable credential with a new signature             |
|                                                     | Notary must go through each registration number type even those who aren't requested |

### Specific Endpoint For Each Registration Number Type

| 👍 Pros                                                                                               | 👎 Cons                                  |
|-------------------------------------------------------------------------------------------------------|------------------------------------------|
| Single verifiable credential                                                                          | Multiple entrypoints for end-users       |
| More semantic value for end-user                                                                      | Multiple verifiable credentials to store |
| Compliant with the new ontology                                                                       |                                          |
| Compatible with OIDC4VC wallets                                                                       |                                          |
| Simplified endpoint input values                                                                      |                                          | 
| Possibility to re-introduce a single endpoint for each registration number type                       |                                          |
| New registration number doesn't require to change existing registration number verifiable credentials |                                          |
| Separated code flow for each registration number type                                                 |                                          |

## Decision

During a development team meeting, on Tuesday the 28th of May 2024, we decided to adopt the last
solution: [Specific Endpoint For Each Registration Number Type](#specific-endpoint-for-each-registration-number-type).

### Problems mentioned during discussion

#### Multiple input/output in API

This does not really comply with the state-of-the-art for REST APIs. An endpoint should have a single purpose.

An easy-to-understand API with 1 parameter, and 1 output is better than an API with optional parameters

#### Implementation over optional parameters

If having easy-to-understand APIs is not enough, having optional parameter forces the code to iterate over them, and
change its behaviour based on whether the parameter is present or not. It creates more complexity and is prone to
errors.

Having 1 mandatory parameter allows to call 1 service and get 1 credential. The flow is linear.

## Consequences

This means that we will be splitting the single `POST - /registrationNumberVC` endpoint into
multiple `GET - /registration-numbers/:type` endpoints to better match REST best practices as those endpoints do not
create/persist any resource, they collect information from third party APIs to return a verifiable credential.

As _GET_ request can't have a request body, each endpoint will have two input values:

- a verifiable credential ID which will override the output's verifiable credential ID if specified
- the value of the corresponding registration number type used in the queries to third party APIs

Of course, the new Gaia-X ontology context will be added to the output verifiable credential.