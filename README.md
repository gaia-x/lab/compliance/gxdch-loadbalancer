# Gaia-X Digital Clearing House

This document contains technical guidelines for service providers aiming to operate a Gaia-X Digital Clearing House.

[[_TOC_]]

## Overview

The [Gaia-X Digital Clearing House (GXDGH)](https://gaia-x.eu/gxdch/) is the mechanism through which Gaia-X is
operationalised in the market.
The [Gaia-X Framework](https://docs.gaia-x.eu/#/framework) contains functional specifications, technical requirements, and
the software to use to become Gaia-X compliant and/or Gaia-X compatible.
The GXDCH contains a subset of the software components in the Gaia-X Framework: the mandatory components and
some of the optional ones.

All the components that go in the GXDCH are open-source, either reused or developed by the Gaia-X Association.

A GXDCH instance runs the engine to validate the Gaia-X rules, therefore becoming the go-to place to become Gaia-X
compliant.
The instances are non-exclusive, interchangeable, and operated by multiple market operators.

> :warning: **There might be a timeframe in which the replicas are desynchronised. Please check
the [Using the GXDCH](#using-the-gxdch) section in this document for details**


Each GXDCH instance must be operated by a service provider according to rules defined with and approved by the Gaia-X
AISBL.
Such providers have then the role
of [Federator](https://docs.gaia-x.eu/technical-committee/architecture-document/22.10/conceptual_model/#federator).
The Gaia-X AISBL is not an operator itself.
Any operator compliant with the requirements defined by the Gaia-X AISBL and featuring the necessary characteristics as
defined by the Gaia-X AISBL can become a GXDCH federator.

## Description of the GXDCH components

The components included in the GXDCH can vary from one release to another, as more
services become available with time. This is the current list of components:

### Mandatory components

* Gaia-X Registry: This service implements the
  [Gaia-X Registry](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_services/#gaia-x-registry)
  as described in the Architecture Document. It is the backbone of the ecosystem
  governance as it stores the Trust Anchors accepted by the Gaia-X Trust Framework,
  the shapes and schemas used to validate the Gaia-X compliance as well as other
  relevant information, like the Terms and Conditions for users of the compliance
  service.

* Gaia-X Compliance: This service implements
  the [Gaia-X Compliance](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_services/#gaia-x-compliance)
  as specified in the Architecture Document.
  It validates Gaia-X Credentials, asserting compliance to the rules described in the compliance document or Trust
  Framework
  and signs ones that are valid.

* Gaia-X notarisation service for the registration number: This service implements
  the [Gaia-X Notary](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_services/#gaia-x-notary-lrn-legal-registration-number).
  This is a tool used to
  check the validity of all registration numbers given by the participants in their Participant Credentials.

### Optional components

* Wizard: UI to sign Verifiable Credentials in a Verifiable Presentation on
  the client side. Calling the Gaia-X Compliance service is integrated with the
  wizard, making it possible to obtain Gaia-X Compliant Credentials by directly
  using this tool.

* Wallet: Application that allows the user to store their Credentials and
  present them to third parties when needed.

* Catalogue: Not available yet

### Versioning

To allow several instances of different versions to be accessible in parallel, we specify paths during the deployment.
That means that `/development` uses the latest `development` image of a component, `/v1` uses the latest `v1` image,
and `/main` uses the latest `main` image.

Please check each component for all the available images and releases.

To ensure consistency, we ask you to use the same convention for your instances.

Each component
provides [several docker images](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/tree/main#images-tags). We
suggest you use the latest stable images if you intend to only deploy a "production-ready" instance. This way, you'll
get minor and fix updates via a simple restart of your deployment.

> :warning: **There is no retro-compatibility between V1 and V2. Both need to be deployed in parallel**


## Installation instructions

> :warning: **This documentation still needs some improvements**

### Hardware Requirements

#### V1 Tagus

The Gaia-X Digital Clearing House uses a limited amount of computing resources when deployed, less than 256MB of RAM for
each instance of registry, compliance, and notary.

The MongoDB used by the registry requires at least 1 vCPU and 600 MB of RAM. We use an attached volume of 8Gio, but 1 or
2Gio should be sufficient as there are 2 collections, with 4000 documents.

You can choose to use your own Mongo Atlas cluster if you want high availability. Please note that the trusted anchors
are retrieved automatically during the registry startup.

#### V2 Loire

Whereas the Loire release is slightly lighter in terms of memory footprint, we still recommend at least 256MB of RAM for
each instance of registry, compliance, and notary.

Please note that there is no longer a MongoDB involved for the `registry`.

### Software Prerequisites

The components are built into docker images, and while it is not a hard requirement, we recommend deploying them on a
Kubernetes cluster of your choice.

#### V1 Tagus

- `gx-registry` requires access to the internet as it will pull SSL and eIDAS certs directly from trusted sources.

- `gaia-x-notary-registrationnumber` requires access to the internet as it will call trusted sources to assert
  registration numbers.

- `gx-compliance` requires access to `gx-registry`. By default, the component will try to call directly inside the
  cluster. You can change this behavior by specifying the `REGISTRY_URL` environment variable.

#### V2 Loire

- `gx-registry` requires access to a DNS resolver to resolve gxdch.eu and all subdomains. It also requires access to a
  Kubo node to pull data from IPFS.
  The [Helm chart](https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/blob/development/k8s/gx-registry/templates/deployment-kubo-peer.yaml?ref_type=heads)
  provided by the lab already takes care of deploying a Kubo peer

- `gaia-x-notary-registrationnumber` requires access to the internet to call trusted sources to assert registration
  numbers. For future development, we recommend adding sticky sessions on the facing ingress.

- `gx-compliance` requires access to `gx-registry`. By default, the component will try to call directly inside the
  cluster. You can change this behavior by specifying the `REGISTRY_URL` environment variable.

#### Certificate requirements

The gx-compliance and gaia-x-notary-registrationnumber will require you to provide SSL key-pair and a certificate issued
from this keypair that will be used to sign the issued credentials and allow validation. The certificate needs to be
either an eIDAS ECert or an EV-SSL, as specified in the Trust Framework. Please check the available options in your
country on how to get such a certificate.

- The private key should be provided in unencrypted [PKCS #8](https://en.wikipedia.org/wiki/PKCS_8) format with line
  feed removed.
- The certificate should be provided in unencrypted [PKCS #8](https://en.wikipedia.org/wiki/PKCS_8) format. Line feed
  can be kept.
- The certificate should include the root CA so that the whole chain can be resolved statically. You can find it
  using [whatsmycert.com](https://whatsmychaincert.com/) by checking the "Include Root Certificate" box for example.

> :warning: **The project does not support PKCS #11 or any else format that PKCS #8**

> :warning: **The certificate should include the root CA. Order matters, leaf at the top, root should be at the bottom.
**
>> leaf - intermediate - CA

In the case of usage of Helm Chart, these variables should be base64 encoded.
Example:

```shell
# Get Base64 encoded key
PRIVATEKEY=$(cat <keyFile>| tr '\n' ' ' | base64 -w 0)
# Get Base64 encoded certificates
CERT=$(cat <caFile> | base64 -w 0)
# Use the variables in the command line
helm upgrade --install -n <namespace> --create-namespace <releaseName> <pathToChart> --set "privateKey=$PRIVATEKEY,x509Certificate=$CERT"
```

### User Requirements

-- TODO: Add any sudo rights or specific user configuration

### Installation steps

The Gaia-X Lab provides a Helm chart for each component to ease installation on a Kubernetes cluster.
You can use it or use your way to deploy components.
The helm chart relies on [cert-manager](https://cert-manager.io/docs/installation/helm/#installing-with-helm) to provide
Let's Encrypt certificates and [nginx](https://kubernetes.github.io/ingress-nginx/deploy/#quick-start) as cluster
ingress

Each component has a section explaining the variables available in the helm chart and an example to deploy a specific
version with helm.

As the compliance service will try to connect to the registry, please follow the order below, first registry, then
compliance, and notary.

1. [Deploy the registry](https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/tree/main#deployment)

```shell
helm upgrade --install -n "v1" --create-namespace gx-registry ./k8s/gx-registry --set "nameOverride=v1,ingress.hosts[0].host=registry.yourdomain.tld,ingress.hosts[0].paths[0].path=/v1,image.tag=v1,ingress.hosts[0].paths[0].pathType=Prefix"
```

2. [Deploy the compliance](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/tree/main#deployment)

```shell
helm upgrade --install -n "v1" --create-namespace gx-compliance ./k8s/gx-compliance --set "nameOverride=v1,ingress.hosts[0].host=compliance.yourdomain.tld,ingress.hosts[0].paths[0].path=/v1,image.tag=v1,ingress.hosts[0].paths[0].pathType=Prefix,privateKey=$complianceKey,X509_CERTIFICATE=$complianceCert"
```

3. [Deploy the notary](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/-/tree/main#deployment)

```shell
helm upgrade --install -n "v1" --create-namespace gx-notary ./k8s/gx-notary --set "nameOverride=v1,ingress.hosts[0].host=registrationnumber.notary.yourdomain.tld,ingress.hosts[0].paths[0].path=/v1,image.tag=v1,ingress.hosts[0].paths[0].pathType=Prefix,privateKey=$privateKey,X509_CERTIFICATE=$x509"
```

Every component should provide an OpenAPI page after deployment under `hostname/path/docs`
e.g. https://compliance.lab.gaia-x.eu/v1/docs

-- TODO: Add test on each component readme and add links here

### Using the GXDCH

During the licensed model operationalization of Gaia-X, the association provides a load balancer in the form of some
HAProxy nodes pointing to Clearing House instances. These HAProxy nodes are managed
through  [Load Balancer ansible scripts](loadbalancer/README.md). The Gaia-X CTO Team is responsible for these scripts
and their update after a clearing house is deployed. You can still prepare a merge request on
the [HAProxy configuration](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/loadbalancer/haproxy.cfg) to include your
GXDCH instance.

The services are accessible behind three subdomains:

- https://registrationnumber.notary.gaia-x.eu/v1/docs/
- https://compliance.gaia-x.eu/v1/docs/
- https://registry.gaia-x.eu/v1/docs/

To check which instance of the clearing house was used, check the "proof" section in the Verifiable Credential received
from the service. The "verificationMethod" field will indicate the domain of the endpoint that was used to sign the
document. The exact endpoints behind the load balancer can then be identified by checking the haproxy configuration file
in this repository.

To check the version of the software deployed on the clearing house, access the endpoint + the major version via a
browser (e.g., registry.lab.gaia-x.eu/v1). The field "version" should match one of the release tags in the code of the
component (e.g., [v1.6.1](https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/tree/v1.6.1?ref_type=tags)).

### Provide a Participant Credential for the GXDCH Status page

Gaia-X provides a [status page](https://docs.gaia-x.eu/framework/?tab=clearing-house) allowing every one to see who are the accredited clearing houses, their status and the version of all the components they deployed.
In order to give even more trust to the end users, we add on this page a checkmark attesting that the GXDCH provided credentials and that they are valid for a given version.
The page will also allow end users to see the Compliance credential emitted for a specific GXDCH.

#### V1 / Tagus

For Tagus, we ask the GXDCH to provide a public URL containing a `VerifiablePresentation`.
This unsigned presentation must contain three signed VerifiableCredentials: `LegalParticipant` `LegalRegistrationNumber` and `TermsAndConditions`.

As the VerifiablePresentation will be submitted to a Compliance Engine (randomly between the GXDCH), the DID and certificate used during the signature process must be public as well.

Several tools and means exist to sign VerifiableCredentials, the [Wizard](https://wizard.lab.gaia-x.eu/stepper) and a [Jupyter Notebook](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101/-/blob/master/gaia-x-101.ipynb). You can also choose to do it manually on your end.

We also provide a [quick video explanation](https://youtu.be/xHaBM-T2--k?feature=shared&t=1535) on how to issue credentials and a [diagram](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/api-usage.md?ref_type=heads#simplified-usage-flow) of this process.

[Example](https://www.delta-dao.com/.well-known/2210_gx_participant.json) of a VerifiablePresentation ready for the Status Page.

Please make sure the file is accessible on your server from `localhost:5137` and `docs.gaia-x.eu/framework` either by disabling cross-origin headers or by adding these two URLs as allowed-origins.

#### V2 / Loire

To be defined